import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

function Books() {
  return (
    <section className="books">
      <Book />
      <Book />
      <Book />
      <Book />
      <Book />
      <Book />
    </section>
  );
}

const Book = () => {
  return (
    <article className="book">
      <CoverImage />
      <Title />
      <Author />
    </article>
  );
};

const CoverImage = () => (
  <img
    width="200"
    src="https://images-eu.ssl-images-amazon.com/images/I/81Vh2dY1uqL._AC_UL200_SR200,200_.jpg"
    alt="resilience"
  />
);

const Title = () => <h1 className="title"> The Art of Resilience </h1>;
const Author = () => <p className="author">by Ross Edgley</p>;

ReactDOM.render(<Books />, document.getElementById("root"));
